#!/usr/local/bin/scopes

using import .glut
using import .gl
using import glm
using import glsl

define usleep
    let lib =
        import-c "sleep.c" "#include <unistd.h>" (list)
    lib.usleep
    

var count = 0
var string = (nullof (pointer i8 'mutable) 'mutable)

glutInit count string
glutInitDisplayMode (| GLUT_DEPTH GLUT_SINGLE GLUT_RGBA)
glutInitWindowPosition 100 100
glutInitWindowSize 500 500
glutCreateWindow "My First openGL Program"

fn compile-shader (shader type)

    let code =
        compile-glsl type
            typify shader

    let val =
        glCreateShader
            match type
                'vertex
                    GL_VERTEX_SHADER
                'fragment
                    GL_FRAGMENT_SHADER
                'geometry
                    GL_GEOMETRY_SHADER
                'compute
                    GL_COMPUTE_SHADER
                else
                    unreachable!;

    
    glShaderSource val 1 (allocaof (code as rawstring)) null
    
    glCompileShader val
    
    var compiled = 0
    var zero = GL_FALSE
    
    glGetShaderiv val GL_COMPILE_STATUS compiled;
    
    if (compiled == zero)
        print code
        error! ("compiliation of " .. (Symbol->string type) .. " shader failed")

    val

unconst
    let vs =
        do
            fn shader ()
                gl_Position = (vec4 0)
            
            compile-shader shader 'vertex

    let fs =
        do
            xvar out color : vec4
            
            fn shader ()
                let x y = gl_FragCoord.x gl_FragCoord.y
                let dis = x * x + y * y
                if (dis > 0.8)
                    discard!;
                color = 
                    vec4 1.0 0.0 0.0 1.0
            
            compile-shader shader 'fragment

    let program =
        do
            let prog = 
                glCreateProgram;
            glAttachShader prog vs
            glAttachShader prog fs
            glLinkProgram prog

            var linked = 0
            var zero = 0
            glGetProgramiv prog GL_LINK_STATUS linked
            if (linked == zero)
                error! "linker error"
            prog



fn run ()
    usleep 1000
    glutPostRedisplay;

fn render ()
    glClearColor 0.0 1.0 0.0 1.0
    glClear GL_COLOR_BUFFER_BIT
    glUseProgram program
    #glDrawArrays GL_TRIANGLE_STRIP 0 4
    glutSwapBuffers;

glutDisplayFunc render
glutIdleFunc run
glutMainLoop;

